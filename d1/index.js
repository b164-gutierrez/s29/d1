const express = require('express');

// Created an application using express.
// in layman's term, app is our server.
const app = express();

const port = 4000;

// Middleware 

app.use(express.json());
app.use(express.urlencoded({ extended:true }))

let users = []

// Routes/endpoint
app.get('/hello', (req, res) => {
	res.send('Hello from the /hello endpoint')
})

// post
app.post("/signup", (req, res) => {
	console.log(req.body);
	//validation
	//If contents of the req.body with the property username and password is not empty, then push the data to the users array. else, please input both username and password
	if(req.body.username !== '' && req.body.password !== '') {
		users.push(req.body);
		res.send(`User ${req.body.username} successfully registered`)
	} else{
		res.send("Please input BOTH username and password")
	}
})



app.put("/change-password", (req, res) => {

	let message;
	for(let i = 0; i < users.length; i++){
		
		//if the username provided in the postman/client and the username of the current object in the loop is the same.
		if(req.body.username === users[i].username){
			
			//change the password of the user found by the loop into the provided in the client.
			users[i].password = req.body.password

			//message response
			message = `User ${req.body.username}'s password has been updated`;

			//breaks out of the loop once a user matches the username provided.
			break;

		}else {
			message = 'User does not exist'
		}
	}

	res.send(message)
})


//activity

// 1 & 2
app.get('/home', (req, res) => {
	res.send('Welcome at HomePage')
})

// 3 & 4
app.get('/users', (req, res) => {
	res.send(req.body)
})

// 5 & 6
app.delete('/delete-user', (req, res) => {
	let message;
	if(users.length != 0){
		for(let i = 0; i<users.length; i++)
		{
			if(req.body.username == users[i].username)
			{
				users.splice(users[i], 1);
				message = `User ${req.body.username} is deleted`;
				break;
			}
		}
	} else{
		message = 'database has no record'
	}

	res.send(message);
})





app.listen(port, () => console.log(`Server running at port: ${port}`));

